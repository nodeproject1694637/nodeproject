variable "region_name" {
  type        = map(string)
  default = {
    default = "us-east-1"
    main = "us-east-1"
    stage = "us-east-2"
    devel = "us-west-1"
  }
  
}

variable "vpc" {
  type        = map(string)
  default = {
    default = "vpc-0bf9672781477db68"
    main = "vpc-0bf9672781477db68"
    stage = "vpc-0fe583d278e7a36b8"
    devel =  "vpc-09539074c2b24cbc7"
  }
  
}


variable "subnets" {
  type        = map(list(string))
  default = {
    default = ["subnet-06f4476f08b3ed123", "subnet-0c927e1b0f4856059"]
    main = ["subnet-06f4476f08b3ed123", "subnet-0c927e1b0f4856059"]
    stage = ["subnet-0221b0be0402724af", "subnet-096d5fe732e46fac9"]
  }
  
}



