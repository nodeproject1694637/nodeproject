resource "aws_security_group" "alb_node" {
  name        = "alb_node"
  description = "secutity group for alb"

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

    ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }


}



output "sg_alb" {
  value       = aws_security_group.alb_node.id
}


resource "aws_security_group" "dev_sg" {
  name        = "dev_sg"
  description = "security group for teh app"

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    security_groups      = [aws_security_group.alb_node.id]

  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }


}



output "sg_app" {
  value       = aws_security_group.dev_sg.id
}
