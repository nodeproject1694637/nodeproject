data "aws_route53_zone" "zone" {
  name         = "sscmva.lat"
}


output "zone_id" {
    value = data.aws_route53_zone.zone.zone_id
}


data "aws_acm_certificate" "cert" {
  domain   = "sscmva.lat"
  statuses = ["ISSUED"]
}


output "certificate_arn" {
    value = data.aws_acm_certificate.cert.arn
}