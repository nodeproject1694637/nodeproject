provider "aws"{
    region = lookup(var.region_name,terraform.workspace)
}

module "sgmodule"{
    source = "./modules/sg"
}

module "r53module"{
    source = "./modules/r53"
}



resource "aws_lb" "my_alb_node" {
  name               = "my-alb-node"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [module.sgmodule.sg_alb]
  subnets            = lookup(var.subnets,terraform.workspace)
}


resource "aws_lb_target_group" "my_target_group_node" {
  name     = "my-target-group"
  port     = 3000
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = lookup(var.vpc,terraform.workspace)
}



resource "aws_lb_listener" "my_listener_node_http" {
  load_balancer_arn = aws_lb.my_alb_node.arn
  port              = 80
  protocol          = "HTTP"
 
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group_node.arn
  }
}


resource "aws_lb_listener" "my_listener_node_https" {
  load_balancer_arn = aws_lb.my_alb_node.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = module.r53module.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group_node.arn
  }
}


resource "aws_ecs_cluster" "my_cluster_node" {
  name = "node-cluster-app"

}


resource "aws_ecs_task_definition" "my_task_node" {
  family = "my-app-node"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 1024
  memory                   = 3072
  container_definitions = jsonencode([
    {
      name      = "node-app"
      image     = "scmv/nodeappdevops"
      cpu       = 1024
      memory    = 3072
      portMappings = [
        {
          containerPort = 3000
          hostPort      = 3000
          appProtocol = "http"
        }
      ]
    }
  ])

}


resource "aws_ecs_service" "my_service_node" {
  name            = "my-service-node-app"
  cluster         = aws_ecs_cluster.my_cluster_node.id
  task_definition = aws_ecs_task_definition.my_task_node.family
  desired_count   = 1
  launch_type = "FARGATE"

  network_configuration {
    subnets = lookup(var.subnets,terraform.workspace)
    assign_public_ip = true 
    security_groups = [module.sgmodule.sg_app]

  }

  load_balancer {
    target_group_arn = aws_lb_target_group.my_target_group_node.arn
    container_name   = "node-app"
    container_port   = 3000
  }


}



resource "aws_route53_record" "node" {
  zone_id = module.r53module.zone_id
  name    = "sscmva.lat"
  type    = "A"
  alias {
    name                   = aws_lb.my_alb_node.dns_name
    zone_id                = aws_lb.my_alb_node.zone_id
    evaluate_target_health = true
  }
}



resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_lb_listener.my_listener_node_http.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group_node.arn
  }

  condition {
    path_pattern {
      values = ["sscmva.lat"]
    }
  }

}




